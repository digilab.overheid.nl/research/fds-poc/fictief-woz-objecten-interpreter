package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

func (app *Application) HandleWozValueDeterminationEvent(ctx context.Context, event *model.Event) error {
	log.WithFields(log.Fields{
		`id`:           event.ID,
		`registeredAt`: event.RegisteredAt,
		`occurredAt`:   event.OccurredAt,
		`subjectIds`:   event.SubjectIDs,
		`eventType`:    event.EventType,
	}).Debug("*** starting HandleWozValueDeterminationEvent")
	data := new(model.WozValueDeterminationEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}
	log.WithFields(log.Fields{
		`data`: fmt.Sprintf("%#v", data),
	}).Debug("*** HandleWozValueDeterminationEvent from event")

	wozObjectValues := []model.WOZObjectValue{}
	if len(event.SubjectIDs) != 1 {
		return fmt.Errorf("WOZ value determination event must have exactly one subject id: %v", event.SubjectIDs)
	}
	wozObjectUuid := event.SubjectIDs[0]
	for _, valuation := range data.Values {
		log.WithFields(log.Fields{}).Debug("Adding WOZ valuation")
		wozObjectValues = append(wozObjectValues, model.WOZObjectValue{
			ID:          uuid.UUID{},
			WOZObjectID: wozObjectUuid,
			Value:       int(valuation.Value),
			ValuationAt: valuation.ValuationAt,
			EffectiveAt: &valuation.EffectiveAt,
		})
	}
	wozObject := model.WOZObject{
		ID:                  wozObjectUuid,
		AddressableObjectID: data.AddressableObjectID,
		RegisteredPeople:    int(data.RegisteredPersons),
		StakeholderOwner:    stakeholderFromEventDataWozSubject(&data.Owner),
		StakeholderOccupant: stakeholderFromEventDataWozSubject(data.Occupant),
		Type:                wozObjectTypeFromEventData(data),
		Values:              wozObjectValues,
	}

	log.WithFields(log.Fields{
		`wozObject`: fmt.Sprintf("%#v", wozObject),
	}).Debug("*** HandleWozValueDeterminationEvent to backend")
	if err := app.Request(ctx, http.MethodPost, "/woz-objects", wozObject, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func stakeholderFromEventDataWozSubject(wozSubject *model.WozSubject) *model.Stakeholder {
	if wozSubject == nil {
		return nil
	}
	if wozSubject.NiNo != 0 {
		return &model.Stakeholder{
			BSN:  strconv.FormatUint(wozSubject.NiNo, 10),
			Type: model.NaturalPerson,
		}
	}
	if wozSubject.Rsin != 0 {
		return &model.Stakeholder{
			RSIN: strconv.FormatUint(wozSubject.Rsin, 10),
			Type: model.NonNaturalPerson,
		}
	}
	return nil
}

func wozObjectTypeFromEventData(data *model.WozValueDeterminationEventData) model.WOZObjectType {
	if strings.EqualFold(data.Type, string(model.Residential)) {
		return model.Residential
	} else {
		return model.NonResidential
	}
}
