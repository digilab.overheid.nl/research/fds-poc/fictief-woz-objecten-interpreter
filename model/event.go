package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

type WozSubject struct {
	NiNo uint64
	Rsin uint64
}

type WozValuation struct {
	Value       uint64    `json:"value"`
	ValuationAt time.Time `json:"valuationAt"`
	EffectiveAt time.Time `json:"effectiveAt"`
}

type WozValueDeterminationEventData struct {
	ID                  string         `json:"id"`
	AddressableObjectID uuid.UUID      `json:"addressableObjectId"`
	Owner               WozSubject     `json:"owner"`
	Occupant            *WozSubject    `json:"occupant"`
	RegisteredPersons   uint64         `json:"registeredPersons"`
	Type                string         `json:"type"`
	Values              []WozValuation `json:"values"`
}
