---
title : "Fictief register WOZ interpreter"
description: "Documentation for the WOZ records interpreter"
lead: ""
date: 2023-08-28T14:52:40+02:00
draft: true
toc: true
---

## Running locally
Clone [this repo](https://gitlab.com/digilab.overheid.nl/research/fds-poc/fictief-woz-objecten-interpreter).

To run the development server:

Default settings are:
- Nats host: localhost:4222
- Base URL of records db: localhost:8080

Run:
```shell
go run . interpreter
```

To overwrite the default settings

Run:
```shell
go run . interpreter --sdg-nats-service-host=127.0.0.1 --sdg-nats-service-port=4222 --sdg-backend-url=http://127.0.0.1:9100
```
